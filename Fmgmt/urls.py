from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

from apps.site import views as site_views
from apps.forum import views as forum_views
from apps.finance import views as finance_views
from apps.files import views as cloud_views



urlpatterns = [

    path('', forum_views.index, name='index'),

    # Auth

    path('admin/', admin.site.urls),

    path('', include('django.contrib.auth.urls')),
    path('logout', site_views.user_logout, name="logout"),
    path('account', site_views.user_account, name="account"),
    path('login', site_views.user_login, name="login"),
    path('register', site_views.user_register, name="register"),
    path('account/edit', site_views.user_edit, name="user-edit"),
    path('account/<str:pk>/delete', site_views.user_delete, name="user-delete"),
    path('account/<slug:slug>', site_views.UserDetailView.as_view(), name="user-detail"),

    # Forum

    path('topics/', forum_views.TopicListView.as_view(), name="topic-list"),
    path('topics/<slug:slug>', forum_views.topic_detail, name="topic-detail"),
    path('topics/<slug:slug>/delete', forum_views.TopicDeleteView.as_view(), name="topic-delete"),
    path('topics/<slug:slug>/edit', forum_views.TopicUpdate.as_view(), name="topic-edit"),
    path('create-topic', forum_views.topic_create, name="topic-create"),

    path('posts/', forum_views.list_recent_posts, name='posts-list-recent'),
    path('posts/new', forum_views.post_create, name="post-create"),
    path('topics/<slug:topic_slug>/<slug:post_slug>', forum_views.post_view, name="post-view"),
    path('posts/<slug:slug>/edit', forum_views.PostUpdate.as_view(), name='post-edit'),
    path('posts/<slug:slug>/delete', forum_views.PostDelete.as_view(), name='post-delete'),

    path('comments/<str:pk>/edit', forum_views.comment_update, name='comment-edit'),
    path('comments/<str:pk>/delete', forum_views.comment_delete, name='comment-delete'),
    path("comments/new/<str:post_pk>", forum_views.comment_create, name='comment-create'),

    # Files

    path("cloud/", cloud_views.cloud_index, name='cloud-index'),
    path("cloud/upload/<slug:slug>", cloud_views.file_upload, name='file-upload'),
    path("cloud/file/<slug:slug>", cloud_views.FileDetailView.as_view(), name="file-detail"),
    path("cloud/file/<slug:slug>/delete", cloud_views.FileDeleteView.as_view(), name="file-delete"),
    path("cloud/file/<slug:slug>/edit", cloud_views.FileEditView.as_view(), name="file-edit"),
    path("cloud/file/<slug:slug>/view", cloud_views.file_view, name="file-view"),
    path("cloud/file/<slug:slug>/visibility", cloud_views.file_visibility, name="file-visibility"),

    path("cloud/<slug:slug>", cloud_views.directory_view, name="directory-view"),
    path("cloud/create_directory/<slug:slug>", cloud_views.directory_create, name="directory-create"),
    path("cloud/<slug:slug>/delete", cloud_views.DirectoryDeleteView.as_view(), name="directory-delete"),

    # Search

    path("search", site_views.search, name="search-page"),

    # Finance

    path('debts', finance_views.debt, name='debts'),
    path('debt/new', finance_views.DebtCreate.as_view(), name='debt'),
    path('debt/<slug:slug>/', finance_views.DebtDetail.as_view(), name='debt-detail'),

    path('earnings', finance_views.earning, name='earnings'),
    path('earning/new', finance_views.EarningCreate.as_view(), name='earning'),
    path('earning/<slug:slug>/', finance_views.EarningDetail.as_view(), name='earning-detail'),

    path('spendings', finance_views.spending, name='spendings'),
    path('spending/new', finance_views.SpendingCreate.as_view(), name='spending'),
    path('spending/<slug:slug>/', finance_views.SpendingDetail.as_view(), name='spending-detail'),

    path('goal/new', finance_views.GoalCreate.as_view(), name='goal'),
    path('budgets', finance_views.budget, name='budgets'),
    path('goal/<slug:slug>/', finance_views.GoalDetail.as_view(), name='goal-detail'),
    path('goal/', finance_views.GoalList.as_view(), name='goalList'),

    path('invest/new', finance_views.InvestCreate.as_view(), name='invest'),
    path('invest/<slug:slug>/', finance_views.InvestDetail.as_view(), name='investment-detail'),
    path('invest/', finance_views.invest, name='investList'),

    path("investment-return/<slug:slug>", finance_views.InvestmentReturnDetail.as_view(), name="investment-return-detail"),
    path("invstment-returns", finance_views.returns, name="returns"),

    path('finance/parse/<slug:slug>', finance_views.parse_file, name="parse-file")

]

if settings.DEBUG == True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += staticfiles_urlpatterns()
