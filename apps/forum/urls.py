# from django.urls import path
# from .views import PostList, PostDetail, PostCreate, PostUpdate, PostDelete, CommentCreate, DebtCreate, DebtDetail,EarningCreate, EarningDetail, SpendingCreate, SpendingDetail, GoalList, GoalCreate, GoalDetail, InvestCreate, InvestDetail
# from . import views
# urlpatterns = [ #paths for redirects
#     path('', PostList.as_view(), name='forum-home'),
#     path('about/', views.about, name='forum-about'),
#
#     path('debt/new', DebtCreate.as_view(), name='debt'),
#     path('budget/', views.budget, name='budget'),
#     path('earning/new', EarningCreate.as_view(), name='earning'),
#     path('spending/new', SpendingCreate.as_view(), name='spending'),
#     path('goal/new', GoalCreate.as_view(), name='goal'),
#     path('invest/new', InvestCreate.as_view(), name='invest'),
#     path('debt/<int:pk>/', DebtDetail.as_view(), name='debt-detail'),
#     path('earning/<int:pk>/', EarningDetail.as_view(), name='earning-detail'),
#     path('spending/<int:pk>/', SpendingDetail.as_view(), name='spending-detail'),
#     path('invest/<int:pk>/', InvestDetail.as_view(), name='investment-detail'),
#     path('goal/<int:pk>/', GoalDetail.as_view(), name='goal-detail'),
#     path('goal/', GoalList.as_view(), name='goalList'),
#     path('debt/', views.debt, name='debtList'),
#     path('earning/', views.earning, name='earningList'),
#     path('spending/', views.spending, name='spendingList'),
#     path('invest/', views.invest, name='investList'),
#
#
#     path('post/new/', PostCreate.as_view(), name='post-create'),
#     path('post/<int:pk>/', PostDetail.as_view(), name='post-detail'),
#     path('post/<int:pk>/update/', PostUpdate.as_view(), name='post-update'),
#     path('post/<int:pk>/delete/', PostDelete.as_view(), name='post-delete'),
#     path('post/comment/', CommentCreate.as_view(), name='add_comment_to_post'),
#
# ]
