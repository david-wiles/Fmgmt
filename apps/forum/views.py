from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from datetime import datetime, timedelta
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.paginator import Paginator
from django.http import HttpResponseNotFound, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.views.decorators.csrf import csrf_protect
from Fmgmt.utils import paginator_catch_exceptions
from apps.files.models import File
from apps.forum.models import Topic, Post, Comment
from apps.site.models import is_moderator
from .forms import CommentForm, PostForm, TopicForm


def index(request):
    # Get the top 10 posts from the last week and display as cards on index page. Re-cache page every hour
    # featured_post = Post.objects.get(title="Forum Presentation")
    top_posts = Post.objects.filter(date_created__gt = (datetime.now() - timedelta(days=7))).order_by("-num_visits")[:10]
    return render(request, 'forum/index.html', context={'posts': top_posts})


def list_recent_posts(request):
    # List all posts, paginated and ordered by date created descending
    page = request.GET.get("page", 1)
    paginator = Paginator(Post.objects.order_by('-date_created'), 20)
    posts = paginator_catch_exceptions(page, paginator)
    return render(request, 'forum/posts/list_recent.html', context={'posts': posts})


# Update a comment or serve comment form if the sender is the comment's owner
@permission_required("forum.change_comment", raise_exception=True)
def comment_update(request, pk):
    # Route for ajax requests to edit comments. Require that users have permission, and checks if user is owner or
    # moderator before updating comment
    comment = Comment.objects.get(id=pk)

    if comment.is_owner(request.user) or is_moderator(request.user):
        if request.method == "POST":
            form = CommentForm(request.POST)
            if form.is_valid():
                comment.text = form.cleaned_data.get("text")
                comment.save()
            return render(request, "forum/comments/comment_view.html", context={"comment": comment})
        else:
            form = CommentForm({'text': comment.text})
            return render(request, 'forum/comments/comment_form.html', context={'form': form})
    else:
        return redirect(comment.post.get_absolute_url())


# Delete comment if the sender is the comment's owner
@csrf_protect
@permission_required("forum.delete_comment", raise_exception=True)
def comment_delete(request, pk):
    # Route for ajax requests to delete a comment. Checks if user is owner or moderator before deleting comment.
    try:
        comment = Comment.objects.get(id=pk)
    except: # Which exception?
        return HttpResponseNotFound("<h1>Not found</h1>")

    if comment.is_owner(request.user) or is_moderator(request.user):
        if request.method == "POST":
            if comment:
                comment.delete()
                return HttpResponse("Comment deleted")

    return HttpResponseNotFound("<h1>Not found</h1")


@permission_required("forum.add_comment", raise_exception=True)
def comment_create(request, post_pk):
    # Route for ajax requests to create a comment. Requires that users have permission.
    post = Post.objects.get(id=post_pk)

    if request.method == "POST":
        form = CommentForm(request.POST)

        if form.is_valid():
            comment = Comment(
                text=form.cleaned_data.get('text'),
                user=request.user,
                post=post
            )
            comment.save()
            return render(request, "forum/comments/comment_view.html", context={"comment": comment})

    return redirect(post.get_absolute_url())


# Create a new topic from topic form, or server the form
@permission_required("forum.add_topic", login_url="/login")
def topic_create(request):
    # Handle topic creation requests. Require that users have permission.
    if request.method == "POST":
        form = TopicForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Topic created successfully!", "alert-success")
            return redirect("/topics")
        else:
            messages.warning(request, "Something wasn't right. Try filling out the form again.", "alert-warning")
            return reverse("topic-create")
    else:
        form = TopicForm()
        return render(request, "forum/topics/create.html", context={"form": form})


class TopicDeleteView(PermissionRequiredMixin, generic.DeleteView):
    # Confirm delete for a topic. Must be a moderator to view
    permission_required = "forum.delete_topic"
    login_url = "/login"
    model = Topic
    template_name = "forum/topics/confirm_delete.html"
    success_url = "/topics"


class TopicListView(generic.ListView):
    # Display all topics as cards
    model = Topic
    context_object_name = "topics"
    template_name = "forum/topics/list_all.html"


class TopicUpdate(PermissionRequiredMixin, generic.UpdateView):
    # Update a topic description or image. Requires that users have permission, and only moderators have this permission
    permission_required = "forum.change_topic"
    login_url = "/login"
    model = Topic
    fields = ['short_description', 'image']
    template_name = "forum/topics/form.html"


# View all posts related to a specific topic
def topic_detail(request, slug):
    # Display all posts for a specific topic, and paginate query results
    topic = Topic.objects.get(slug=slug)
    page = request.GET.get('page', 1)
    paginator = Paginator(topic.post_set.all(), 20)
    posts = paginator_catch_exceptions(page, paginator)

    return render(request, 'forum/posts/list_all.html',
                  context={'topic': topic, "posts": posts})


def post_view(request, topic_slug, post_slug):
    # Get a post from topic slug and post slug, increase the view count by one, and paginate comments
    topic = Topic.objects.get(slug=topic_slug)
    post = Post.objects.get(slug=post_slug, topic=topic.id)
    post.num_visits = post.num_visits + 1
    post.save()

    page = request.GET.get('page', 1)
    paginator = Paginator(post.comment_set.all(), 20)
    comments = paginator_catch_exceptions(page, paginator)

    return render(request, 'forum/posts/post_view.html',
                  context={'post': post, 'topic': topic, "comments": comments})


# Create a new post from post form, or serve post form
@permission_required("forum.add_post", login_url="/login")
def post_create(request):
    # Handle form creation requests. Require that users have permission.
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = Post(
                user=request.user,
                title=form.cleaned_data.get('title'),
                image=form.cleaned_data.get("image"),
                link=form.cleaned_data.get('link'),
                text=form.cleaned_data.get('text'),
                topic=form.cleaned_data.get('topic'),
            )
            post.save()
            messages.success(request, "Post created successfully!", "alert-success")
            return redirect(post.get_absolute_url())
        else:
            messages.warning(request, "Something wasn't right. Try filling out the form again.", "alert-danger")
            return reverse("post-create")

    else:
        form = PostForm()
        form.fields["image"].queryset = File.objects.filter(user=request.user, is_public=True)
        return render(request, "forum/posts/post_form.html", context={'form': form})


class PostUpdate(PermissionRequiredMixin, generic.UpdateView):
    # Form for editing of post text, image, or link. Requires that users have permission and are either the owner
    # or moderators
    permission_required = "forum.change_post"
    login_url = "/login"
    model = Post
    fields = ['text', 'image', 'link']
    template_name = "forum/posts/post_form.html"

    def get(self, request, *args, **kwargs):
        if self.get_object().is_owner(request.user):
            response = super(PostUpdate, self).get(self, request, *args, **kwargs)
            response.context_data["form"].fields["image"].queryset = File.objects.filter(user=request.user, is_public=True)
            return response
        elif is_moderator(request.user):
           return super(PostUpdate, self).post(self, request, *args, **kwargs)
        else:
            messages.error(request, "Not allowed.", "alert-danger")
            return redirect(self.object.get_absolute_url())

    def post(self, request, *args, **kwargs):
        if self.get_object().is_owner(request.user) or is_moderator(request.user):
            messages.success(request, "Post updated!", "alert-success")
            return super(PostUpdate, self).post(self, request, *args, **kwargs)
        else:
            messages.error(request, "Not allowed.", "alert-danger")
            return redirect(self.object.get_absolute_url())


class PostDelete(PermissionRequiredMixin, UserPassesTestMixin, generic.DeleteView):
    # Confirmation to delete a post. Requires that users have permission and are either the owner or moderator
    permission_required = "forum.delete_post"
    login_url = "/login"
    model = Post
    template_name = "forum/posts/confirm_delete.html"
    success_url = reverse_lazy('posts-list-recent')

    def test_func(self):
        if self.get_object().is_owner(self.request.user) or is_moderator(self.request.user):
            return True
        else:
            messages.error(self.request, "Not allowed.", "alert-danger")
            return redirect(self.object.get_absolute_url())

    def post(self, request, *args, **kwargs):
        messages.success(request, "Post deleted!", "alert-success")
        return super(PostDelete, self).post(self, request, *args, **kwargs)
