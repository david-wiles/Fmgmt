from django import forms
from apps.files.models import File
from .models import Comment, Post, Topic


class CommentForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea())


class PostForm(forms.Form):
    title = forms.CharField(max_length=255)
    image = forms.ModelChoiceField(queryset=File.objects.filter(is_public=True), required=False)
    text = forms.CharField(widget=forms.Textarea())
    link = forms.CharField(max_length=1024, required=False)
    topic = forms.ModelChoiceField(queryset=Topic.objects.all())


class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = ("title", "image", "short_description",)
