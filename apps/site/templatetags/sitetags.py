from django import template
from django.contrib.auth.models import Group
from apps.site.models import FmgmtUser

register = template.Library()

@register.filter(name="is_moderator")
def is_moderator(user):
    return user.groups.filter(name="moderator").exists()
