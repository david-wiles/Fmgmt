from django.contrib import messages
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, UserChangeForm
from django.contrib.postgres.search import SearchQuery, SearchRank
from django.core.paginator import Paginator
from django.db.models import F
from django.shortcuts import render, redirect
from django.views import generic

from Fmgmt.utils import paginator_catch_exceptions
from apps.files.models import File
from apps.forum.models import Post
from apps.site.forms import CustomUserCreationForm, CustomUserChangeForm
from apps.site.models import FmgmtUser, is_moderator


def search(request):
    # Full text search for records matching text
    results = None
    if (request.GET.get("text") is not None):
        query = SearchQuery(request.GET.get("text"))
        rank = SearchRank(F("tokens"), query)

        posts = Post.objects.annotate(rank=rank).filter(tokens=query).order_by("-rank")

        # topics = Topic.objects.annotate(rank=rank).filter(tokens=query).order_by("-rank")

        page = request.GET.get('page', 1)
        paginator = Paginator(posts, 20)
        results = paginator_catch_exceptions(page, paginator)

    return render(request, "site/search/search.html", context={"results": results})


@login_required
def user_logout(request):
    # Logout user
    logout(request)
    return redirect('/')


@login_required
def user_account(request):
    # View user account from logged in user
    return render(request, 'site/auth/account.html')


def user_login(request):
    # User log in
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, "Logged in!", "alert-success")
            return redirect('/')
        else:
            messages.info(request, "No account found with that username.", "alert-info")
            return redirect('/register')
    else:
        form = AuthenticationForm()

    return render(request, 'site/auth/login.html', {'form': form})


def user_register(request):
    # User register
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, "User successfully created!", "alert-success")
            return redirect('/')
        else:
            messages.warning(request, "Something wasn't right. Try filling out the form again.", "alert-warning")
            return redirect("/register")
    else:
        form = CustomUserCreationForm()

    return render(request, 'site/auth/register.html', {'form': form})


@login_required
def user_edit(request):
    # Form to edit the currently logged in user
    if request.method == "POST":
        form = CustomUserChangeForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, "Account details updated!", "alert-success")
            return redirect("/account")
        else:
            messages.warning(request, "Something wasn't right. Try filling out the form again.", "alert-warning")
    else:
        form = CustomUserChangeForm({
            "email": request.user.email,
            "first_name": request.user.first_name,
            "last_name": request.user.last_name,
            "image": request.user.image,
            "bio": request.user.bio
        })

    return render(request, 'site/auth/edit.html', {'form': form})


@login_required
def user_delete(request, pk):
    # Makes user account inactive. Checks if the user is the currently logged in user or is moderator
    user = FmgmtUser.objects.get(id=pk)
    if request.method == "POST":
        if user.is_logged_in_user(request.user) or is_moderator(request.user):
            user.is_active = False
            user.save()
            messages.success(request, "User deleted!", "alert-success")
            return redirect("/")
        else:
            messages.error(request, "Not allowed.", "alert-danger")
            return redirect("/")
    else:
        if user.is_logged_in_user(request.user) or is_moderator(request.user):
            return render(request, "site/auth/confirm_delete.html", {'user_id': user.id})
        else:
            messages.error(request, "Not allowed.", "alert-danger")
            return redirect("/")


class UserDetailView(generic.DetailView):
    # View a specific user's account
    model = FmgmtUser
    template_name = "site/auth/detail.html"
