from django.contrib.auth.models import Group, Permission
from django.core.management import BaseCommand


GROUP_NAMES = ["basic_user", "moderator"]
MODELS = ["post", "topic", "comment", "file", "directory", "user"]

# Create default groups and permissions. Moderator has almost identical permissions as basic user,
# but is able to act on objects which he doesn't own, whereas basic users can only act on their own objects
class Command(BaseCommand):
    help = "Creates the two groups used in the site by default"

    def handle(self, *args, **kwargs):
        for group in GROUP_NAMES:
            new_group, created = Group.objects.get_or_create(name=group)
            for model in MODELS:
                name = f"Can view {model}"
                try:
                    perm = Permission.objects.get(name=name)
                except Permission.DoesNotExist:
                    print(f"Permission {name} not found")
                new_group.permissions.add(perm)

            PERMISSIONS = []
            try:
                PERMISSIONS.append(Permission.objects.get(name="Can add post"))
                PERMISSIONS.append(Permission.objects.get(name="Can add comment"))
                PERMISSIONS.append(Permission.objects.get(name="Can add file"))
                PERMISSIONS.append(Permission.objects.get(name="Can add directory"))
                PERMISSIONS.append(Permission.objects.get(name="Can change post"))
                PERMISSIONS.append(Permission.objects.get(name="Can change comment"))
                PERMISSIONS.append(Permission.objects.get(name="Can change file"))
                PERMISSIONS.append(Permission.objects.get(name="Can change directory"))
                PERMISSIONS.append(Permission.objects.get(name="Can change user"))
                PERMISSIONS.append(Permission.objects.get(name="Can delete post"))
                PERMISSIONS.append(Permission.objects.get(name="Can delete comment"))
                PERMISSIONS.append(Permission.objects.get(name="Can delete file"))
                PERMISSIONS.append(Permission.objects.get(name="Can delete directory"))
                if group == "moderator":
                    PERMISSIONS.append(Permission.objects.get(name="Can add topic"))
                    PERMISSIONS.append(Permission.objects.get(name="Can change topic"))
                    PERMISSIONS.append(Permission.objects.get(name="Can delete topic"))
            except Permission.DoesNotExist:
                print("Uh oh")

            for perm in PERMISSIONS:
                new_group.permissions.add(perm)

    print("Done creating default groups!")
