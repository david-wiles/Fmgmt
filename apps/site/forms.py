from django.contrib.auth.forms import UserCreationForm, UsernameField, UserChangeForm
from django.forms import EmailField

from apps.site.models import FmgmtUser


class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = FmgmtUser
        fields = ("username", "email")
        field_classes = {'username': UsernameField, 'email': EmailField}


class CustomUserChangeForm(UserChangeForm):
    password = None

    class Meta:
        model = FmgmtUser
        fields = ("email", "first_name", "last_name", "image", "bio")
        field_classes = {'email': EmailField}
