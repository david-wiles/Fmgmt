from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from apps.site.forms import CustomUserCreationForm, CustomUserChangeForm
from apps.site.models import FmgmtUser


class NewUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = FmgmtUser
    list_display = ['email', 'username']


admin.site.register(FmgmtUser, NewUserAdmin)
