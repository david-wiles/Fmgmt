from django.contrib import admin
from apps.finance.models import Debt, Earning, Investment, Spending, Goal

admin.site.register(Debt)
admin.site.register(Earning)
admin.site.register(Investment)
admin.site.register(Spending)
admin.site.register(Goal)
