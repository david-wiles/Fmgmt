from django import forms
from .models import Debt, Earning, Spending, Goal, Investment, InvestmentReturn


class DebtForm(forms.ModelForm):
    class Meta:
        model = Debt
        fields = ['amount', 'type', 'borrowee']

class EarningForm(forms.ModelForm):
    class Meta:
        model = Earning
        fields = ['type', 'year', 'wage']

class SpendingForm(forms.ModelForm):
    class Meta:
        model = Spending
        fields = ['amount', 'year', 'type', 'reason']

class GoalForm(forms.ModelForm):
    class Meta:
        model = Goal
        fields = ['goal', 'budget']

class InvestmentForm(forms.ModelForm):
    class Meta:
        model = Investment
        fields = ['amount', 'type']
