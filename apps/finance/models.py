import uuid
import datetime
from django.db import models
from django.utils import timezone
from django.utils.text import slugify

from apps.site.models import FmgmtUser
from django.urls import reverse


class Earning(models.Model):
    type = models.CharField(max_length=255)
    year = models.BigIntegerField(default=0)
    wage = models.BigIntegerField(default=0)
    slug = models.SlugField(max_length=512, unique=True, editable=False)
    date = models.DateTimeField(auto_now_add=True)
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    user = models.ForeignKey(FmgmtUser, default=0, related_name='earning', on_delete=models.CASCADE)


    def __str__(self):
        return self.type

    def save(self, *args, **kwargs):
        # Creates a slug based on type and date created if one doesn't exist
        if self.slug == '':
            self.slug = slugify(self.type + str(datetime.datetime.now()))
        super(Earning, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('earning-detail', kwargs={"slug": self.slug})


class Goal(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    user = models.ForeignKey(FmgmtUser, default=0, related_name='goal', on_delete=models.CASCADE)
    slug = models.SlugField(max_length=512, unique=True, editable=False)
    goal = models.CharField(max_length=255)
    budget = models.BigIntegerField(default=0)

    def __str__(self):
        return self.goal

    def save(self, *args, **kwargs):
        # Creates a slug based on user and date created if one doesn't exist
        if self.slug == '':
            self.slug = slugify(str(self.user) + str(datetime.datetime.now()))
        super(Goal, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('goal-detail', kwargs={"slug": self.slug})


class Debt(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    user = models.ForeignKey(FmgmtUser, default=0, related_name='debt', on_delete=models.CASCADE)
    type = models.CharField(max_length=255)
    slug = models.SlugField(max_length=512, unique=True, editable=False)
    borrowee = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)
    amount = models.BigIntegerField(default=0)

    def __str__(self):
        return self.type

    def save(self, *args, **kwargs):
        # Creates a slug based on user and date created if one doesn't exist
        if self.slug == '':
            self.slug = slugify(self.type + str(datetime.datetime.now()))
        super(Debt, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('debt-detail', kwargs={"slug": self.slug})


class Spending(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    user = models.ForeignKey(FmgmtUser, default=0, related_name='spending', on_delete=models.CASCADE)
    amount = models.BigIntegerField(default=0)
    type = models.CharField(max_length=255)
    slug = models.SlugField(max_length=512, unique=True, editable=False)
    reason = models.CharField(max_length=255)
    year = models.BigIntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.type

    def save(self, *args, **kwargs):
        # Creates a slug based on type and date created if one doesn't exist
        if self.slug == '':
            self.slug = slugify(self.type + str(datetime.datetime.now()))
        super(Spending, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('spending-detail', kwargs={"slug": self.slug})


class Investment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    user = models.ForeignKey(FmgmtUser, default=0, related_name='investment', on_delete=models.CASCADE)
    type = models.CharField(max_length=255)
    slug = models.SlugField(max_length=512, unique=True, editable=False)
    amount = models.BigIntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.type

    def save(self, *args, **kwargs):
        # Creates a slug based on type and date created if one doesn't exist
        if self.slug == '':
            self.slug = slugify(self.type + str(datetime.datetime.now()))
        super(Investment, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('investment-detail', kwargs={"slug": self.slug})


class InvestmentReturn(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    user = models.ForeignKey(FmgmtUser, default=0, related_name='investment_return', on_delete=models.CASCADE)
    investment = models.ForeignKey(Investment, related_name="investment_return", null=False, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=512, unique=True, editable=False)
    current_value = models.BigIntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.investment.type

    def save(self, *args, **kwargs):
        # Creates a slug based on user and date created if one doesn't exist
        if self.slug == '':
            self.slug = slugify(str(self.user) + str(datetime.datetime.now()))
        super(InvestmentReturn, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("investment-return-detail", kwargs={"pk": self.pk})
