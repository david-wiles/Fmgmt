from django.shortcuts import render, get_object_or_404, redirect
from .models import Debt, Earning, Spending, Goal, Investment, InvestmentReturn
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import F
from .forms import DebtForm, EarningForm, SpendingForm, GoalForm, InvestmentForm
from django.contrib.auth.models import User
from chartit import DataPool, Chart, PivotDataPool, PivotChart


class DebtCreate(LoginRequiredMixin, CreateView):
    model = Debt
    form_class = DebtForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class DebtDetail(DetailView):
    model = Debt


class DebtList(ListView):
    model = Debt


def debt(request):
    debt = DataPool(  # what data is being retrieved and where it is being retrieved from
        series=[
            {'options': {'source': Debt},
             'terms': [{'type': 'type', 'amount': 'amount'}]},
        ]
    )

    cht = Chart(
        datasource=debt,
        series_options=[{'options': {'type': 'column', 'stacking': False},
                         'terms': {'type': ['amount']}}],
        chart_options={'title': {'text': 'Your Debt'},
                       'xAxis': {'title': {'text': 'type'}},
                       'yAxis': {'title': {'text': 'Net amount'}},
                       'legend': {'enabled': True},
                       'credits': {'enabled': True}},

    )
    return render(request, 'finance/userDebt.html', {'chart_list': [cht]})


class EarningCreate(LoginRequiredMixin, CreateView):
    model = Earning
    form_class = EarningForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class EarningDetail(DetailView):
    model = Earning


def earning(request):
    earning = DataPool(  # what data is being retrieved and where it is being retrieved from
        series=[
            {'options': {'source': Earning},
             'terms': [{'wage': 'wage', 'year': 'year'}]},
        ]
    )

    cht = Chart(
        datasource=earning,
        series_options=[{'options': {'type': 'line', 'stacking': False},
                         'terms': {'year': ['wage']}}],
        chart_options={'title': {'text': 'Your Income'},
                       'xAxis': {'title': {'text': 'year'}},
                       'yAxis': {'title': {'text': 'wage'}},
                       'legend': {'enabled': True},
                       'credits': {'enabled': True}},

    )
    return render(request, 'finance/userEarning.html', {'chart_list': [cht]})


class SpendingCreate(LoginRequiredMixin, CreateView):
    model = Spending
    form_class = SpendingForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class SpendingDetail(DetailView):
    model = Spending


class SpendingList(ListView):
    model = Spending


def spending(request):
    spending = DataPool(  # what data is being retrieved and where it is being retrieved from
        series=[
            {'options': {'source': Spending},
             'terms': [{'type': 'type', 'amount': 'amount'}]},
        ]
    )

    cht = Chart(
        datasource=spending,
        series_options=[{'options': {'type': 'column', 'stacking': False},
                         'terms': {'type': ['amount']}}],
        chart_options={'title': {'text': 'Your Spending'},
                       'xAxis': {'title': {'text': 'type'}},
                       'yAxis': {'title': {'text': 'amount'}},
                       'legend': {'enabled': True},
                       'credits': {'enabled': True}},

    )
    return render(request, 'finance/userSpending.html', {'chart_list': [cht]})


class GoalCreate(LoginRequiredMixin, CreateView):
    model = Goal
    form_class = GoalForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class GoalDetail(DetailView):
    model = Goal


class GoalList(ListView):
    model = Goal
    template_name = 'finance/goal_list.html'
    context_object_name = 'posts'


class InvestCreate(LoginRequiredMixin, CreateView):
    model = Investment
    form_class = InvestmentForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class InvestDetail(DetailView):
    model = Investment


def invest(request):
    investment = DataPool(  # what data is being retrieved and where it is being retrieved from
        series=[
            {'options': {'source': Investment},
             'terms': [{'type': 'type', 'amount': 'amount'}]},
        ]
    )

    cht = Chart(
        datasource=investment,
        series_options=[{'options': {'type': 'pie', 'stacking': False},
                         'terms': {'type': ['amount']}}],
        chart_options={'title': {'text': 'Your Investments'},
                       'xAxis': {'title': {'text': 'type'}},
                       'yAxis': {'title': {'text': 'Net amount'}},
                       'legend': {'enabled': True},
                       'credits': {'enabled': True}},
    )
    return render(request, 'finance/userInvest.html', {'chart_list': [cht]})


def budget(request):
    debt = DataPool(  # what data is being retrieved and where it is being retrieved from
        series=[{'options': {'source': Earning},
                 'terms': [{'wage': 'wage', 'year': 'year'}]},
                {'options': {'source': Spending},
                 'terms': [{'amount': 'amount', 'year2': 'year'}]}]

    )

    cht = Chart(
        datasource=debt,
        series_options=[{'options': {'type': 'column',
                                     'stacking': False},
                         'terms': {'year': ['wage'], 'year2': ['amount']}}],
        chart_options={'title': {
            'text': 'Your Income vs your spending'},
            'xAxis': {'title': {'text': 'Income and Spending'}},
            'yAxis': {'title': {'text': 'amount'}},
            'legend': {'enabled': True},
            'credits': {'enabled': True}},

    )
    return render(request, 'finance/userBudget.html', {'chart_list': [cht]})


class InvestmentReturnDetail(DetailView):
    model = InvestmentReturn


def returns(request):
    returns = DataPool(
        series=[
            {
                "options": {
                    "source": InvestmentReturn
                },
                "terms" : [{
                    "amount": "amount",
                    "year": "year",
                }]
            }
        ]
    )

    cht = Chart(
        datasource=returns,
        series_options=[
            {
                "options": {
                    "type": "column",
                    "stacking": False
                },
                "terms": {
                    "amount": ["amount"],
                    "year": ["year"]
                }
            }
        ],
        chart_options={
            "title": {
                "text": "Investment Returns"
            },
            "xAxis": {
                "title": {
                    "text" : "Year"
                }
            },
            "yAxis": {
                "title": {
                    "text": "Amount"
                }
            },
            "legend": {
                "enabled": True
            },
        }
    )
    return render(request, 'finance/returns.html', {'chart_list': [cht]})



def parse_file(request, slug):
    """
    Parse a user-uploaded file and insert contents into database
    :param request:
    :param slug:    Relative filename
    :return:        Success or error banner
    """
    pass