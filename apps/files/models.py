import datetime
import os
import uuid
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from django.utils.text import slugify
from apps.site.models import FmgmtUser, get_user_file_dir


class File(models.Model):
    upload = models.FileField(upload_to=get_user_file_dir)
    description = models.CharField(max_length=2048, null=True)

    id = models.UUIDField(default=uuid.uuid1, primary_key=True, editable=False)
    slug = models.SlugField(max_length=255, unique=True, editable=False)
    directory = models.ForeignKey("Directory", on_delete=models.CASCADE, null=False)
    user = models.ForeignKey("site.FmgmtUser", on_delete=models.CASCADE, null=False)
    date_modified = models.DateTimeField(auto_now=True, null=False, editable=False)
    date_created = models.DateTimeField(auto_now_add=True, editable=False)
    is_public = models.BooleanField(null=False, default=False)

    class Meta:
        ordering = ["date_created"]

    def is_owner(self, user: FmgmtUser):
        return user == self.user

    def filename(self) -> str:
        parts = (self.upload.name).split("/")
        return parts[-1]

    def get_short_date(self) -> str:
        return self.date_modified.strftime("%b %d, %H:%M:%S")

    def get_absolute_url(self):
        return reverse("file-detail", args=[str(self.slug)])

    def get_view_url(self) -> str:
        return f"/cloud/file/{str(self.slug)}/view"

    def save(self, *args, **kwargs):
        if self.slug == '':
            self.slug = slugify(str(self.upload) + str(datetime.datetime.now()))
        super(File, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.filename())


@receiver(models.signals.post_delete, sender=File)
def auto_delete_file(sender, instance, **kwargs):
    if instance.upload:
        if os.path.isfile(instance.upload.path):
            os.remove(instance.upload.path)


class Directory(models.Model):
    name = models.CharField(max_length=255, null=False)

    id = models.UUIDField(default=uuid.uuid1, primary_key=True, editable=False)
    slug = models.SlugField(max_length=255, editable=False)
    parent_dir = models.ForeignKey("Directory", on_delete=models.CASCADE, null=True)
    user = models.ForeignKey("site.FmgmtUser", on_delete=models.CASCADE, null=False)
    date_created = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        ordering = ['date_created']

    def is_owner(self, user: FmgmtUser):
        return user == self.user

    def get_short_date(self) -> str:
        return self.date_created.strftime("%b %d")

    def get_absolute_url(self):
        return reverse("directory-view", args=[str(self.slug)])

    def save(self, *args, **kwargs):
        if self.slug == '':
            self.slug = slugify(self.name)
        super(Directory, self).save(*args, **kwargs)

    def __str__(self):
        return f"{self.name} ({self.get_short_date()})"
