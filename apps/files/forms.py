from django import forms

# Forms for models in file cloud

class FileForm(forms.Form):
    upload = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    description = forms.CharField(max_length=2048)


class DirectoryCreate(forms.Form):
    name = forms.CharField(max_length=255)
