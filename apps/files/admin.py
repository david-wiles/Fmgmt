from django.contrib import admin
from .models import File, Directory

admin.site.register(File)
admin.site.register(Directory)

class FileAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug", ("title",)}


class DirectoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug", ("title",)}
