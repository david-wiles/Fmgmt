INVALID
/*
create extension if not exists "uuid-ossp";

create table django_migrations
(
    id      serial                   not null
        constraint django_migrations_pkey
            primary key,
    app     varchar(255)             not null,
    name    varchar(255)             not null,
    applied timestamp with time zone not null
);

alter table django_migrations
    owner to postgres;

create table django_content_type
(
    id        serial       not null
        constraint django_content_type_pkey
            primary key,
    app_label varchar(100) not null,
    model     varchar(100) not null,
    constraint django_content_type_app_label_model_76bd3d3b_uniq
        unique (app_label, model)
);

alter table django_content_type
    owner to postgres;

create table auth_permission
(
    id              serial       not null
        constraint auth_permission_pkey
            primary key,
    name            varchar(255) not null,
    content_type_id integer      not null
        constraint auth_permission_content_type_id_2f476e4b_fk_django_co
            references django_content_type
            deferrable initially deferred,
    codename        varchar(100) not null,
    constraint auth_permission_content_type_id_codename_01ab375a_uniq
        unique (content_type_id, codename)
);

alter table auth_permission
    owner to postgres;

create index auth_permission_content_type_id_2f476e4b
    on auth_permission (content_type_id);

create table auth_group
(
    id   serial       not null
        constraint auth_group_pkey
            primary key,
    name varchar(150) not null
        constraint auth_group_name_key
            unique
);

alter table auth_group
    owner to postgres;

create index auth_group_name_a6ea08ec_like
    on auth_group (name);

create table auth_group_permissions
(
    id            serial  not null
        constraint auth_group_permissions_pkey
            primary key,
    group_id      integer not null
        constraint auth_group_permissions_group_id_b120cbf9_fk_auth_group_id
            references auth_group
            deferrable initially deferred,
    permission_id integer not null
        constraint auth_group_permissio_permission_id_84c5c92e_fk_auth_perm
            references auth_permission
            deferrable initially deferred,
    constraint auth_group_permissions_group_id_permission_id_0cd325b0_uniq
        unique (group_id, permission_id)
);

alter table auth_group_permissions
    owner to postgres;

create index auth_group_permissions_group_id_b120cbf9
    on auth_group_permissions (group_id);

create index auth_group_permissions_permission_id_84c5c92e
    on auth_group_permissions (permission_id);

create table site_forumuser
(
    password     varchar(128)             not null,
    last_login   timestamp with time zone,
    is_superuser boolean                  not null,
    username     varchar(150)             not null
        constraint site_forumuser_username_key1
            unique,
    first_name   varchar(30)              not null,
    last_name    varchar(150)             not null,
    email        varchar(254)             not null,
    is_staff     boolean                  not null,
    is_active    boolean                  not null,
    date_joined  timestamp with time zone not null,
    id           uuid                     not null
        constraint site_forumuser_pkey1
            primary key,
    slug         varchar(50)              not null
        constraint site_forumuser_slug_key1
            unique,
    bio          text,
    image_id     uuid
);

alter table site_forumuser
    owner to postgres;

create index site_forumuser_username_ff0a299d_like
    on site_forumuser (username);

create index site_forumuser_slug_40c5cfe8_like
    on site_forumuser (slug);

create index site_forumuser_image_id_373f62f3
    on site_forumuser (image_id);

create table site_forumuser_groups
(
    id           serial  not null
        constraint site_forumuser_groups_pkey
            primary key,
    forumuser_id uuid    not null
        constraint site_forumuser_group_forumuser_id_986f8410_fk_site_foru
            references site_forumuser
            deferrable initially deferred,
    group_id     integer not null
        constraint site_forumuser_groups_group_id_73cfb421_fk_auth_group_id
            references auth_group
            deferrable initially deferred,
    constraint site_forumuser_groups_forumuser_id_group_id_b4f1e47f_uniq
        unique (forumuser_id, group_id)
);

alter table site_forumuser_groups
    owner to postgres;

create index site_forumuser_groups_forumuser_id_986f8410
    on site_forumuser_groups (forumuser_id);

create index site_forumuser_groups_group_id_73cfb421
    on site_forumuser_groups (group_id);

create table site_forumuser_user_permissions
(
    id            serial  not null
        constraint site_forumuser_user_permissions_pkey
            primary key,
    forumuser_id  uuid    not null
        constraint site_forumuser_user__forumuser_id_dcf1bcc8_fk_site_foru
            references site_forumuser
            deferrable initially deferred,
    permission_id integer not null
        constraint site_forumuser_user__permission_id_206ca1af_fk_auth_perm
            references auth_permission
            deferrable initially deferred,
    constraint site_forumuser_user_perm_forumuser_id_permission__417b551a_uniq
        unique (forumuser_id, permission_id)
);

alter table site_forumuser_user_permissions
    owner to postgres;

create index site_forumuser_user_permissions_forumuser_id_dcf1bcc8
    on site_forumuser_user_permissions (forumuser_id);

create index site_forumuser_user_permissions_permission_id_206ca1af
    on site_forumuser_user_permissions (permission_id);

create table file_cloud_directory
(
    name          varchar(255)             not null,
    id            uuid                     not null
        constraint file_cloud_directory_pkey
            primary key,
    slug          varchar(255)             not null,
    date_created  timestamp with time zone not null,
    parent_dir_id uuid
        constraint file_cloud_directory_parent_dir_id_e578d5e8_fk_file_clou
            references file_cloud_directory
            deferrable initially deferred,
    user_id       uuid                     not null
        constraint file_cloud_directory_user_id_8c4aa889_fk_site_forumuser_id
            references site_forumuser
            deferrable initially deferred
);

alter table file_cloud_directory
    owner to postgres;

create index file_cloud_directory_slug_cd6a08ec
    on file_cloud_directory (slug);

create index file_cloud_directory_slug_cd6a08ec_like
    on file_cloud_directory (slug);

create index file_cloud_directory_parent_dir_id_e578d5e8
    on file_cloud_directory (parent_dir_id);

create index file_cloud_directory_user_id_8c4aa889
    on file_cloud_directory (user_id);

create table file_cloud_file
(
    upload        varchar(100)             not null,
    description   varchar(2048),
    id            uuid                     not null
        constraint file_cloud_file_pkey
            primary key,
    slug          varchar(255)             not null
        constraint file_cloud_file_slug_key
            unique,
    date_modified timestamp with time zone not null,
    date_created  timestamp with time zone not null,
    is_public     boolean                  not null,
    directory_id  uuid                     not null
        constraint file_cloud_file_directory_id_27f74f3f_fk_file_clou
            references file_cloud_directory
            deferrable initially deferred,
    user_id       uuid                     not null
        constraint file_cloud_file_user_id_6b086c3d_fk_site_forumuser_id
            references site_forumuser
            deferrable initially deferred
);

alter table file_cloud_file
    owner to postgres;

alter table site_forumuser
    add constraint site_forumuser_image_id_373f62f3_fk_file_cloud_file_id
        foreign key (image_id) references file_cloud_file
            deferrable initially deferred;

create index file_cloud_file_slug_2f89f6e0_like
    on file_cloud_file (slug);

create index file_cloud_file_directory_id_27f74f3f
    on file_cloud_file (directory_id);

create index file_cloud_file_user_id_6b086c3d
    on file_cloud_file (user_id);

create table forum_app_topic
(
    title             varchar(255) not null
        constraint forum_app_topic_title_key1
            unique,
    image             varchar(100),
    short_description varchar(255) not null,
    id                uuid         not null
        constraint forum_app_topic_pkey1
            primary key,
    slug              varchar(255) not null
        constraint forum_app_topic_slug_key1
            unique,
    date_created      date         not null,
    tokens            tsvector
);

alter table forum_app_topic
    owner to postgres;

create index forum_app_topic_title_f50e0280_like
    on forum_app_topic (title);

create index forum_app_topic_slug_e9640d46_like
    on forum_app_topic (slug);

create trigger topic_update_trigger
    before insert or update
    on forum_app_topic
    for each row
execute procedure pg_catalog.tsvector_update_trigger();

create table forum_app_post
(
    title         varchar(255)             not null,
    text          text                     not null,
    id            uuid                     not null
        constraint forum_app_post_pkey1
            primary key,
    slug          varchar(512)             not null
        constraint forum_app_post_slug_key1
            unique,
    num_visits    integer                  not null,
    date_modified timestamp with time zone not null,
    date_created  timestamp with time zone not null,
    topic_id      uuid                     not null
        constraint forum_app_post_topic_id_dad328bf_fk_forum_app_topic_id
            references forum_app_topic
            deferrable initially deferred,
    user_id       uuid                     not null
        constraint forum_app_post_user_id_effe1468_fk_site_forumuser_id
            references site_forumuser
            deferrable initially deferred,
    image_id      uuid
        constraint forum_app_post_image_id_82e09cb8_fk_file_cloud_file_id
            references file_cloud_file
            deferrable initially deferred,
    link          varchar(1024),
    tokens        tsvector
);

alter table forum_app_post
    owner to postgres;

create index forum_app_post_slug_1e160a9e_like
    on forum_app_post (slug);

create index forum_app_post_topic_id_dad328bf
    on forum_app_post (topic_id);

create index forum_app_post_user_id_effe1468
    on forum_app_post (user_id);

create index forum_app_post_image_id_82e09cb8
    on forum_app_post (image_id);

create trigger post_update_trigger
    before insert or update
    on forum_app_post
    for each row
execute procedure pg_catalog.tsvector_update_trigger();

create table forum_app_comment
(
    text          text                     not null,
    id            uuid                     not null
        constraint forum_app_comment_pkey
            primary key,
    date_modified timestamp with time zone not null,
    date_created  timestamp with time zone not null,
    post_id       uuid                     not null
        constraint forum_app_comment_post_id_8b4e0cc7_fk_forum_app_post_id
            references forum_app_post
            deferrable initially deferred,
    user_id       uuid                     not null
        constraint forum_app_comment_user_id_3354e17d_fk_site_forumuser_id
            references site_forumuser
            deferrable initially deferred
);

alter table forum_app_comment
    owner to postgres;

create index forum_app_comment_post_id_8b4e0cc7
    on forum_app_comment (post_id);

create index forum_app_comment_user_id_3354e17d
    on forum_app_comment (user_id);

create table django_admin_log
(
    id              serial                   not null
        constraint django_admin_log_pkey
            primary key,
    action_time     timestamp with time zone not null,
    object_id       text,
    object_repr     varchar(200)             not null,
    action_flag     smallint                 not null
        constraint django_admin_log_action_flag_check
            check (action_flag >= 0),
    change_message  text                     not null,
    content_type_id integer
        constraint django_admin_log_content_type_id_c4bce8eb_fk_django_co
            references django_content_type
            deferrable initially deferred,
    user_id         uuid                     not null
        constraint django_admin_log_user_id_c564eba6_fk_site_forumuser_id
            references site_forumuser
            deferrable initially deferred
);

alter table django_admin_log
    owner to postgres;

create index django_admin_log_content_type_id_c4bce8eb
    on django_admin_log (content_type_id);

create index django_admin_log_user_id_c564eba6
    on django_admin_log (user_id);

create table django_session
(
    session_key  varchar(40)              not null
        constraint django_session_pkey
            primary key,
    session_data text                     not null,
    expire_date  timestamp with time zone not null
);

alter table django_session
    owner to postgres;

create index django_session_session_key_c0390e0f_like
    on django_session (session_key);

create index django_session_expire_date_a5c62663
    on django_session (expire_date);
*/
